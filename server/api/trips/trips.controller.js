var Trips = require("../../database").Trips;
var Places_Visiting = require("../../database").Places_Visiting;

var config = require("../../config");
var moment = require('moment');


exports.createTrip = function (req, res) {
    var tripBody = req.body.trip;
    var t = moment(tripBody.startDate, "DD-MM-YYYY").format("YYYY-MM-DD");
    var t2 = moment(tripBody.endDate).format("YYYY-MM-DD");
    console.log(t2);
    //var t2 = moment(tripBody.endDate + " " + tripBody.endTime + " +0000", "DD-MM-YYYY HH:mm Z");

    Trips
        .create({
            users_id: 1,
            start_date: t,
            days: tripBody.days,
            end_date: t2,
            country_origin: tripBody.origin,
            name: tripBody.name

        }).then(function (record) {
            res.status(200).json(record);
        }).catch(function (err) {
            res.status(500).json(err);
        });

};

exports.createItem = function (req, res) {
    var itemBody = req.body.item;
    var t = moment(itemBody.start_date, "DD-MM-YYYY").format("YYYY-MM-DD");
    var t2 = moment(itemBody.end_date, "DD-MM-YYYY").format("YYYY-MM-DD");
    //var t2 = moment(tripBody.endDate + " " + tripBody.endTime + " +0000", "DD-MM-YYYY HH:mm Z");

    Places_Visiting
        .create({
            trips_id: itemBody.trips_id,
            start_date: t,
            start_time: itemBody.start_time,
            end_date: t2,
            end_time: itemBody.end_time,
            country_origin: itemBody.origin,
            places_name: itemBody.name

        }).then(function (record) {
            res.status(200).json(record);
        }).catch(function (err) {
            res.status(500).json(err);
        });

};

exports.retrieveTrip = function (req, res) {

    Trips
        .findOne({
            where: {
                id: req.query.searchString
            }
        }).then(function (result) {
            // if (result.data != null) {
            //     for (var i = 0; i < result.data.length; i++) {
            //         result.data.start_date[i] = moment(result.data.start_date[i]).format("DD-MM-YYYY");
            //         // result.data.startTime[i] = moment(result.data.start_date[i]).format("HH:mm");
            //     }
            // }
            res
                .status(200)
                .json(result);

        }).catch(function (err) {
            res
                .status(500)
                .json(err);
        })

};

exports.retrieveTripList = function (req, res) {

    Trips
        .findAll({
            where: {
                users_id: req.query.searchString
            }
        }).then(function (result) {
            res
                .status(200)
                .json(result);

        }).catch(function (err) {
            res
                .status(500)
                .json(err);
        })

};

exports.retrieveItemList = function (req, res) {
    var t = moment(req.query.day, "D MMM YY").format("YYYY-MM-DD");
    Places_Visiting
        .findAll({
            where: {
                trips_id: req.query.trips_id,
                start_date: t,
                booking_status: null
                // booking_status: {
                //     $ne: "deleted"
                // }
            }
        }).then(function (result) {
            res
                .status(200)
                .json(result);

        }).catch(function (err) {
            res
                .status(500)
                .json(err);
        })

};

exports.updateItem = function (req, res) {
    var where = {};
    where.id = req.params.id;
    Places_Visiting
        .update({ places_name: req.body.name },
        { where: where }
        ).then(function (result) {
            res
                .status(200)
                .json(result);
        }).catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json(err);
        })

};

exports.deleteItem = function (req, res) {
    var where = {};
    where.id = req.params.id;
    Places_Visiting
        .update({ booking_status: "deleted" },
        { where: where }
        ).then(function (result) {
            res
                .status(200)
                .json(result);
        }).catch(function (err) {
            console.log(err);
            res
                .status(500)
                .json(err);
        })

};