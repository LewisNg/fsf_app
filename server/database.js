var Sequelize = require('sequelize');
var config = require("./config");

console.log(config.mysql);
var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var Users = require("./models/users.model.js")(database);
var Trips = require("./models/trips.model.js")(database);
var Places_Categories = require("./models/places_categories.model.js")(database);
var Places = require("./models/places.model.js")(database);
var Places_Visiting = require("./models/places_visitings.model.js")(database);
// BEGIN: MYSQL RELATIONS
Users.hasMany(Trips, {foreignKey: 'users_id'});
Trips.hasMany(Places_Visiting, {foreignKey: 'trips_id'});
Places.hasMany(Places_Visiting, {foreignKey: 'places_id'});
Places_Categories.hasMany(Places, {foreignKey: 'places_categories_id'});
// Trips.belongsTo(Users, {foreignKey:'id'});
// END: MYSQL RELATIONS
// force: config.seed
// database
//     .sync({force: config.seed})
//     .then(function () {
//         console.log("Database in Sync Now");
//         require("./seed")();
//     });

module.exports = {
    Users: Users,
    Trips: Trips,
    Places_Categories: Places_Categories,
    Places: Places,
    Places_Visiting: Places_Visiting
};

