
var config = require("./config");
var database = require("./database");
var Users = database.Users;

module.exports = function () {
    if (config.seed) {
        var hashpassword = "Password@123";
        Users
            .create({
                username: "admin",
                password: hashpassword,
                firstName: "Admin",
                lastName: "Admin",
                addressLine1: "79 02 Ayer Rajah crescent",
                addressLine2: "",
                city: "Singapore",
                email: "admin@test.com",
                postcode: "42312",
                isAdmin: true,
                phone: "98832587"
            })
            .then(function (user) {
                console.log(user);
            }).catch(function () {
                console.log("Error", arguments)
            })

        Users
            .create({
                username: "normaluser",
                password: hashpassword,
                firstName: "Normal",
                lastName: "User",
                addressLine1: "",
                addressLine2: "",
                city: "Singapore",
                email: "user@test.com"
            })
            .then(function (user) {
                console.log(user);
            }).catch(function () {
                console.log("Error", arguments)
            })

        // Friends
        //     .create({
        //         user1_email: "user@test.com",
        //         user2_email: "admin@test.com",
        //         status: "confirmed"
        //     })
        //     .then(function (user) {
        //         console.log(user);
        //     }).catch(function () {
        //         console.log("Error", arguments)
        //     })
        
        // User
        //     .create({
        //         username: "kenken64",
        //         password: hashpassword,
        //         firstName: "Kenneth",
        //         lastName: "Phang",
        //         addressLine1: "79 02 Ayer Rajah Crescent",
        //         addressLine2: "",
        //         city: "Singapore",
        //         email: "kenneth.phang@gmail.com",
        //         postcode: "42312",
        //         phone: "98832587",
        //         google: "http://google.com",
        //         facebook: "http://facebook.com",
        //         twitter: "http://twitter.com"
        //     })
        //     .then(function (user) {
        //         Post
        //             .create({
        //                 caption: "Looks like a scene from Hollywood movie!",
        //                 url: "1477564902212_4.jpg"
        //             })
        //             .then(function (post) {
        //                 user
        //                     .addPost(post)
        //                     .then(function () {
        //                         console.log("Done");
        //                     })
        //                     .catch(function () {
        //                     });
        //             })
        //             .catch(function () {

        //             });Post
        //             .create({
        //                 caption: "Aaaaah!!!!!",
        //                 url: "1477564908055_5.jpg"
        //             })
        //             .then(function (post) {
        //                 user
        //                     .addPost(post)
        //                     .then(function () {
        //                         console.log("Done");
        //                     })
        //                     .catch(function () {
        //                     });
        //             })
        //             .catch(function () {

        //             });Post
        //             .create({
        //                 caption: "The DAY!",
        //                 url: "1477564913469_6.jpg"
        //             })
        //             .then(function (post) {
        //                 user
        //                     .addPost(post)
        //                     .then(function () {
        //                         console.log("Done");
        //                     })
        //                     .catch(function () {
        //                     });
        //             })
        //             .catch(function () {

        //             });Post
        //             .create({
        //                 caption: "Cute :*",
        //                 url: "1477564919836_7.jpg"
        //             })
        //             .then(function (post) {
        //                 user
        //                     .addPost(post)
        //                     .then(function () {
        //                         console.log("Done");
        //                     })
        //                     .catch(function () {
        //                     });
        //             })
        //             .catch(function () {

        //             });
        //     })
        //     .catch(function () {
        //         console.log("Error", arguments)
        //     })

    }
};