var Sequelize = require("sequelize");
var moment = require('moment');
module.exports = function (database) {
    return database.define('places_visitings', {
        trips_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            references: {
                model: 'trips',
                key: 'id'
            }
        },
        places_id: {
            type: Sequelize.INTEGER(11),
            allowNull: true,
            references: {
                model: 'places',
                key: 'id'
            }
        },
        places_name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        start_date: {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        start_time: {
            type: Sequelize.STRING,
            allowNull: true
        },
        end_date: {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        end_time: {
            type: Sequelize.STRING,
            allowNull: true
        },
        booking_status: {
            type: Sequelize.STRING,
            allowNull: true
        },
        file_path: {
            type: Sequelize.DATEONLY,
            allowNull: true
        }
    },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
};