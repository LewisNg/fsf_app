var Sequelize = require("sequelize");
var moment = require('moment');
module.exports = function (database) {
    return database.define('places', {
        name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        country: {
            type: Sequelize.STRING,
            allowNull: true
        },
        region: {
            type: Sequelize.STRING,
            allowNull: true
        },
        city: {
            type: Sequelize.STRING,
            allowNull: true
        },
        postal_code: {
            type: Sequelize.STRING,
            allowNull: true
        },
        geolocation: {
            type: Sequelize.STRING,
            allowNull: true
        },
        places_categories_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            references: {
                model: 'places_categories',
                key: 'id'
            }
        }
    },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
};