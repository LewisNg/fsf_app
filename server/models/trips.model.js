var Sequelize = require("sequelize");
var moment = require('moment');
module.exports = function (database) {
    return database.define('trips', {
        users_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        // start_date: {
        //     type: Sequelize.STRING,
        //     allowNull: true
        // },
        start_date: {
            type: Sequelize.DATEONLY,
            // get: function () {
            //     return moment.utc(this.getDataValue('start_date')).format('DD-MM-YYYY');
            // },
            allowNull: true
        },
        days: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        end_date: {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true
        },
        country_origin: {
            type: Sequelize.STRING,
            allowNull: true
        },
        city_origin: {
            type: Sequelize.STRING,
            allowNull: true
        },
        photo: {
            type: Sequelize.BLOB,
            allowNull: true
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
};