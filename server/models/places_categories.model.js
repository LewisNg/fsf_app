var Sequelize = require("sequelize");
var moment = require('moment');
module.exports = function (database) {
    return database.define('places_categories', {
        name: {
            type: Sequelize.STRING,
            allowNull: true
        }
    },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
};