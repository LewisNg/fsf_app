var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('users', {
        email: {
            type:Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: true
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        addressLine1: Sequelize.STRING,
        addressLine2: Sequelize.STRING,
        city: Sequelize.STRING,
        postcode: Sequelize.STRING,
        phone: Sequelize.STRING,
        reset_password_token: {
            type: Sequelize.STRING,
            allowNull: true
        },
        remember_created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        sign_in_count: {
            type: Sequelize.INTEGER(11),
            allowNull: true
        },
        current_sign_in_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        last_sign_in_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        current_sign_in_ip: {
            type: Sequelize.STRING,
            allowNull: true
        },
        last_sign_in_ip: {
            type: Sequelize.STRING,
            allowNull: true
        },
        isAdmin: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        reset_password_sent_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        deleted_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    },
    {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
    });
};