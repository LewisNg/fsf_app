'use strict';

// var UserController = require("./api/user/user.controller");
var express = require("express");
var config = require("./config");

var TripsController = require("./api/trips/trips.controller");

// const API_USERS_URI = "/api/users";
// const HOME_PAGE = "/home.html#!/weddinggram";
// const SIGNIN_PAGE = "/home.html#!/signIn";

module.exports = function (app) {

    app.post("/api/trips/create-trip", TripsController.createTrip);
    app.post("/api/trips/create-item", TripsController.createItem);
    app.get("/api/trips/retrieve-trip", TripsController.retrieveTrip);
    app.get("/api/trips/retrieve-trip-list", TripsController.retrieveTripList);
    app.get("/api/trips/retrieve-item-list", TripsController.retrieveItemList);
    app.put("/api/trips/update-item/:id", TripsController.updateItem);
    app.put("/api/trips/delete-item/:id", TripsController.deleteItem);
    app.use(express.static(__dirname + "/../client/"));

};
