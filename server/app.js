var express = require("express");
var bodyParser = require('body-parser');
var path = require('path');

var config = require("./config");

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


//require('./auth.js')(app);
require("./routes")(app);
// require("./database");

app.listen(config.port, function () {
    console.log("Server running at http://localhost:" + config.port);
});