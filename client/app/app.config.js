(function () {
    angular
        .module("travelApp")
        .config(travelAppConfig);
    travelAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function travelAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("create_trip", {
                url: "/create_trip",
                views: {
                    "content": {
                        templateUrl: "../app/planner/create_trip.html"
                    }
                },
                controller: 'CreateTripCtrl',
                controllerAs: 'ctrl'
            })
            .state("planner", {
                url: "/planner",
                views: {
                    "content": {
                        templateUrl: "../app/planner/planner.html"
                    }
                },
                controller: 'PlannerCtrl',
                controllerAs: 'ctrl',
                params: {
                    obj: null
                }
            })
        $urlRouterProvider.otherwise("/create_trip");
    }
})();
