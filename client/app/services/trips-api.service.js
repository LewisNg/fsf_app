(function () {
    angular
        .module("travelApp")
        .service("TripService", TripService);

    TripService.$inject = ['$http'];

    function TripService($http) {

        var service = this;

        service.createTrip = createTrip;
        service.createItem = createItem;
        service.retrieveTrip = retrieveTrip;
        service.retrieveTripList = retrieveTripList;
        service.retrieveItemList = retrieveItemList;
        service.updateItem = updateItem;
        service.deleteItem = deleteItem;

        function createTrip(trip) {
            return $http({
                method: 'POST'
                , url: 'api/trips/create-trip'
                , data: { trip: trip }
            });
        }

        function createItem(item) {
            return $http({
                method: 'POST'
                , url: 'api/trips/create-item'
                , data: { item: item }
            });
        }

        function retrieveTrip(searchString) {
            return $http({
                method: 'GET',
                url: "api/trips/retrieve-trip",
                params: { 'searchString': searchString }
            });
        }

        function retrieveTripList(searchString) {
            return $http({
                method: 'GET',
                url: "api/trips/retrieve-trip-list",
                params: { 'searchString': searchString }
            });
        }

        function retrieveItemList(trips_id, day) {
            return $http({
                method: 'GET',
                url: "api/trips/retrieve-item-list",
                params: { 'trips_id': trips_id, 'day': day }
            });
        }

        function updateItem(id, name) {
            return $http({
                method: 'PUT',
                url: "/api/trips/update-item/" + id,
                data: {
                    name: name
                }
            });
        }
        function deleteItem(id) {
            return $http({
                method: 'PUT',
                url: "/api/trips/delete-item/" + id
            });
        }
    }
})();