(function () {
    angular
        .module("travelApp")
        .controller("PlannerCtrl", PlannerCtrl)

    PlannerCtrl.$inject = ['$stateParams', 'moment', 'TripService'];

    function PlannerCtrl($stateParams, moment, TripService) {
        var vm = this;
        this.insertItem = insertItem;

        vm.trip = {
            name: "",
            start_date: "",
            end_date: ""
        };

        vm.item = {
            name: "",
            start_date: "",
            start_time: "",
            end_date: "",
            end_time: ""
        };

        vm.status = {
            message: ""
            , code: ""
        };

        vm.editText = "Edit";
        vm.editing = false;

        initDetails();
        function initDetails() {
            console.log($stateParams.obj);
            vm.item.trips_id = $stateParams.obj;
            TripService
                .retrieveTrip($stateParams.obj)
                .then(function (result) {
                    vm.trip = result.data;
                    vm.trip.start_date = moment(result.data.start_date, "YYYY-MM-DD").format("D MMM YY");
                    //var ed = moment(result.data.start_date, "DD-MM-YYYY").add(result.data.days, 'd');
                    vm.trip.end_date = moment(result.data.end_date, "YYYY-MM-DD").format("D MMM YY");
                    vm.daySelected = 1;
                    vm.dateSelected = vm.trip.start_date;
                    initPlanner();
                    // if (result.data != null) {
                    //     for (var i = 0; i < result.data.length; i++) {
                    //         vm.items[i].startDate = moment(result.data[i].start_date).get('time');
                    //         vm.items[i].startTime = moment(result.data[i].start_date).date();
                    //     }
                    // }
                    // console.log(vm.items);

                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                })
        }

        function initPlanner() {
            //var day = vm.dateSelected || 1;
            console.log(vm.dateSelected);
            TripService
                .retrieveItemList($stateParams.obj, vm.dateSelected)
                .then(function (result) {
                    vm.itemList = result.data;
                    vm.itemList.start_date = moment(result.data.start_date).format("D MMM YY");
                    vm.itemList.end_date = moment(result.data.end_date).format("D MMM YY");
                    console.log(vm.itemList);
                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                })
        }

        vm.nextDay = function () {
            if (vm.dateSelected < vm.trip.end_date) {
                vm.daySelected += 1;
                vm.dateSelected = moment(vm.dateSelected, "D MMM YY").add(1, 'd').format("D MMM YY");
                initPlanner();
            }
        };
        vm.previousDay = function () {
            if (vm.dateSelected > vm.trip.start_date) {
                vm.daySelected -= 1;
                vm.dateSelected = moment(vm.dateSelected, "D MMM YY").subtract(1, 'd').format("D MMM YY");
                initPlanner();
            }
        };

        vm.editItem = function (index) {
            if (vm.editText == "Edit") {
                vm.editText = "Save";
                vm.editing = true;
            } else if (vm.editText == "Save") {
                console.log(index);
                TripService
                    .updateItem(vm.itemList[index].id, vm.itemList[index].places_name)
                    .then(function (result) {
                        console.log("result " + JSON.stringify(result));
                        vm.editText = "Edit";
                        vm.editing = false;
                        initPlanner();
                    })
                    .catch(function (err) {
                        console.log("error " + JSON.stringify(err));
                        vm.status.message = err.data.name;
                        vm.status.code = err.data.parent.errno;
                    });
            }
        }

        vm.cancelItem = function () {
            vm.editText = "Edit";
            vm.editing = false;
            initPlanner();
        }

        vm.deleteItem = function (index) {
            TripService
                .deleteItem(vm.itemList[index].id)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    initPlanner();
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        }
        function insertItem() {
            TripService
                .createItem(vm.item)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    initPlanner();
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        }
    }
})();