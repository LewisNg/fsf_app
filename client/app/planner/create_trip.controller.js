(function () {
    angular
        .module("travelApp")
        .controller("CreateTripCtrl", CreateTripCtrl)

    CreateTripCtrl.$inject = ['$state', 'moment', 'TripService'];

    function CreateTripCtrl($state, moment, TripService) {
        var vm = this;
        this.createTrip = createTrip;

        vm.trip = {

            startDate: "",
            days: "",
            origin: "",
            name: ""
        };

        vm.status = {
            message: ""
            , code: ""
        };

        initDetails();

        function initDetails() {
            TripService
                .retrieveTripList(1)
                .then(function (result) {
                    vm.list = result.data;

                }).catch(function (error) {
                    console.log(JSON.stringify(error));
                })
        }
        function createTrip() {
            vm.trip.endDate = moment(vm.trip.startDate, "DD-MM-YYYY").add((vm.trip.days - 1), 'd');
            console.log(vm.trip.endDate);
            TripService
                .createTrip(vm.trip)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $state.go("planner", { obj: result.data.id });
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        }
    }
})();