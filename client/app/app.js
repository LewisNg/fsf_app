(function () {
    angular
        .module("travelApp", [
            "ui.router", 'angularMoment'
        ]);
})();